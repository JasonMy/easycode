package cn.lu.project.generate;


import cn.lu.project.generate.entity.GenerateProperties;
import cn.lu.project.generate.service.BuildJavaProject;
import cn.lu.project.generate.service.DataBaseAutoImport;
import cn.lu.project.generate.service.ReadConfiguration;
import cn.lu.project.generate.service.ReadExcel;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: easycode
 * @description: 启动类
 * @author: zhlu
 * @create: 2020-04-07 16:30
 * @Introduction(说明背景故事)
 **/
@Slf4j
public class EasycodeApplication {

    public static void main(String[] args) {
        ReadConfiguration readConfiguration = new ReadConfiguration();
        GenerateProperties properties = readConfiguration.getGenerateProperties();

        ReadExcel readExcel = new ReadExcel(properties);
        if (properties.isOutputScriptFile()) {
            readExcel.outputFile();
            log.info("SQL文件写出成功");
        }

        if (properties.isAutoRunScript()) {
            DataBaseAutoImport autoImport = new DataBaseAutoImport(properties);
            autoImport.execute(readExcel.getScriptList());
            log.info("创建数据表成功");
        }

        if (properties.isBuildJava()) {
            BuildJavaProject.build(properties);
        }

        log.info("程序结束");
    }

}
