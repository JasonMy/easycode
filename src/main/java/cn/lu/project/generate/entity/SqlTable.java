package cn.lu.project.generate.entity;

import lombok.Data;

import java.util.List;

/**
 * @program: easycode
 * @description: SQL
 * @author: zhlu
 * @create: 2020-04-07 16:28
 * @Introduction(说明背景故事)
 **/
@Data
public class SqlTable {

    private String tableName;
    private String tableDesc;

    private List<ExcelRow> rows;

}
