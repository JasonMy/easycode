package cn.lu.project.generate.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @program: easycode
 * @description: Excel
 * @author: zhlu
 * @create: 2020-04-07 16:27
 * @Introduction(说明背景故事)
 **/
@Data
public class ExcelRow {

    @Excel(name = "No")
    private String no;

    @Excel(name = "Logical Name")
    private String desc;

    @Excel(name = "Physical Name")
    private String field;

    @Excel(name = "Data Type")
    private String dataType;

    @Excel(name = "Key")
    private String key;

    @Excel(name = "Not Null")
    private String notNull;

    @Excel(name = "Default")
    private String defaultVal;
}
