package cn.lu.project.generate.entity;

import lombok.Data;

/**
 * @program: easycode
 * @description: SQL
 * @author: zhlu
 * @create: 2020-04-07 16:27
 * @Introduction(说明背景故事)
 **/
@Data
public class GenerateProperties {

    private String excelPath;

    /**
     * 导出的SQL脚本路径地址
     */
    private String exportSqlDir;

    /**
     * 是否写出脚本到文件
     */
    private boolean outputScriptFile;

    /**
     * 是否自动运行SQL脚本
     */
    private boolean autoRunScript;

    /**
     * 配置数据库
     */
    private DataSource dataSource;

    /**
     * 构建Java项目的文件
     */
    private BuildConf buildConf;

    /**
     * 是否构建Java项目文件
     */
    private boolean buildJava = false;

    @Data
    public static class DataSource {
        private String host;
        private String database;
        private Integer port = 3306;
        private String user;
        private String password;
        private String driverName;
        private String tablePrefix;
    }

    @Data
    public static class BuildConf{
        private String packageName;
        private String author;
        private String excludeTable;
        private String modularName;
        private boolean restControllerStyle;
        private String srcMainJava;
        private String resources;
    }
}
